<?php
//no need for routing link with the same name as the view/controller file
//just place them for reminders


return array(
	'_root_'  => 'home/index',  // The default route
	'_404_'   => 'home/404',    // The main 404 route

	'login' => 'home/login',
	'home'  => 'home/home',
	'profile'  => 'home/profile',


	//for the backend routing
	//no need for routing link with the same name as the view/controller file
	//'backend/index' => 'backend/index',
	//'backend/home' => 'backend/home',
	//'backend/logout' => 'backend/logout',

	//individual pages.. adding them to the routing
	'superuser' => 'backend/superuser',
	'backend/add-admin-staff' => 'backend/addadminstaff',
	'backend/reset-user-password' => 'backend/resetuserpass',
	'backend/add-course' => 'backend/addcourse',
	'backend/manage-course-subject' => 'backend/managecoursesubject',
	'backend/edit-subject' => 'backend/editsubjects',
	'backend/add-student'  => 'backend/addstudent',
	'backend/manage-student-subject'  => 'backend/managestudentsubject',
	'backend/add-subjects-to-instructors' => 'backend/addsubjectsinstrc',

	//announcement and news
	// 'backend/announcement' => 'backend/announcement',
	//'backend/news' => 'backend/news',
	'backend/manage-announ-news' => 'backend/manageannounnews',

	//rooms and schedule
	//'backend/addrooms' => 'backend/addrooms',
	//'backend/addsched' => 'backend/addsched',
	'backend/manage-schedule' => 'backend/manageschedule',



);