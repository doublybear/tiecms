<?php
use \Model\Admin;
class Controller_Home extends Controller_Template
{
	public function __construct(){
		//a short config file to restrict access based on usertype
		Util::noAccess();
	}

	public function action_index(){
		$data['news'] = Model_Admin::getNews();

		$this->template->title = 'TIE - Course Management System';
		$this->template->content = View::forge('home/index', $data);
	}

	public function action_home(){

		$data = null;
		
		//check if session is available
		if(null !== Session::get('userid')){
			$data['userdetail'] = Model_Student::getUserDetail( Session::get('userid') , Session::get('usertype') );
		}
		$data['news'] = Model_Admin::getNews();
		$this->template->title = "TIE - Home";
		$this->template->content = View::forge('home/home', $data);
	}

	public function action_new(){
		$data['news'] = Model_Admin::getNews();
		$this->template->title = "TIE - News Bulletin";
		$this->template->content = View::forge('home/new', $data);

	}

	public function action_404(){
		$this->template->title = "TIE - 404 - Page Not Found";
		$this->template->content = View::forge('home/404');
	}

	public function action_login(){


		$this->template->title = "TIE - Log-in Page";
		$this->template->content = View::forge('home/login');

		if(isset($_POST['submitLogin']))
		{
			//set validation rule
			$val = Validation::forge('login');
			$val->add('loginUsername','Username')->add_rule('required');
			$val->add('loginPassword','Password')->add_rule('required');
			//set custom message
			$val->set_message('required', ':label cannot be empty');

			//run validation
			if($val->run()){

				//get user input
				$username = $_POST['loginUsername'];
				$password = $_POST['loginPassword'];

				//get user password to compare to the password in $_post['loginPassword']
				$userData = Model_Student::getUserPassword($username);
				//check if account type is student. student access
				if ($userData['userType'] === 'student') {
					//call compare password method
					$result = Util::checkUserCredentials($userData , $password);
				}
				else{
					//add session to the site
					Session::set_flash('error','Your Account Don\'t belong here.');
	    			//redirect user to the home page but with credectials
	    			Response::redirect('/login');
				}
				
				if($result)
				{
					//add session to the site
					Session::set(array('userid' => $userData['userId'] , 'usertype' => $userData['userType']));
	    			//redirect user to the home page but with credectials
	    			Response::redirect('home');
				}
				else
				{
					Session::set_flash('error','Wrong username or password');
				}
			}
			else{
				foreach ($val->error() as $key => $value) {
					Session::set_flash('error',$value->get_message());
				}
			}
		}
	}

	public function action_logout(){
		Session::destroy();
		Response::redirect('/');
	}

}