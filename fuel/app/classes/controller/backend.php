<?php
use \Model\Admin;
class Controller_Backend extends Controller_Template
{
	public function __construct(){
		//a short config file to restrict access based on usertype
		Util::noAccess();
	}

	public function action_index(){
		$this->template->title = 'Admin TIE - Course Management System';
		$this->template->content = View::forge('backend/index');

		if(isset($_POST['submitLogin']))
		{
			//validate form fields
			//$val = $this->validate($_POST['submitLogin']);

			//get user input
			$username = $_POST['loginUsername'];
			$password = $_POST['loginPassword'];

			//get user password to compare to the password in $_post['loginPassword']
			$userData = Model_Admin::getUserPassword($username);

			//check if account type is student. student access only
				if ($userData['userType'] != 'student') {
					//call compare password method
					$result = Util::checkUserCredentials($userData , $password);
				}
				else{
					//add session to the site
					Session::set_flash('error','Your Account Doesn\'t belong here.');
	    			//redirect user to the home page but with credectials
	    			Response::redirect('backend/login');
				}
			//call compare password method
			
			
			if($result)
			{
				//add session to the site
				Session::set(array('userid' => $userData['userId'] , 'usertype' => $userData['userType']));
    			//redirect user to the home page but with credectials
    			Response::redirect('backend/home');
			}
			else
			{
				Session::set_flash('error','Wrong username or password');
			}
		}
	}

	public function action_404(){
		$this->template->title = "TIE - 404 - Page Not Found";
		$this->template->content = View::forge('home/404');
	}

	public function action_logout(){
		Session::destroy();
		Response::redirect('/backend');
	}

	public function action_home(){
		
		$data = null;
		//check if session is available
		if(null !== Session::get('userid')){
			$data['userdetail'] = Model_Admin::getUserDetail( Session::get('userid') , Session::get('usertype') );
		}

		// add super user page in home view
		if(Session::get('usertype') === 'admin')
		{
			$data['usertypepage'] = View::forge('backend/superuser');
		}
		// add faculty page in home view
		else
		{
			$data['usertypepage'] = View::forge('backend/faculty');
		}

		$this->template->title = "TIE - Home";
		$this->template->content = View::forge('backend/home', $data);
		
	}

	public function action_addadminstaff(){
		
		$this->template->title = "TIE - Add Admin Staff";
		$this->template->content = View::forge('backend/superdir/addadminstaff');

		// get form post values
		if(isset($_POST['submitAccount']))
		{
			$result = Model_Admin::insertAdminOrStaff($_POST);

			if($result)
				{
					//add session to the site
					Session::set_flash('success','User Saved.');
	    			//redirect user to the home page but with credectials
	    			Response::redirect('backend/add-admin-staff');
				}
				else
				{
					Session::set_flash('error','Opps! Something went wrong. Retry entry');
				}
		}
	}

	public function action_resetuserpass(){
		$this->template->title = "TIE - Reset User Password";
		$this->template->content = View::forge('backend/superdir/resetuserpass');

		//check if user exist
		if(isset($_POST['checkUser']))
		{
			//checkUser ( 'userId')
			$result = Model_Admin::checkUser($_POST['loginuserid']);
		
			if ($result) {
				$data['result'] = $result;
				$this->template->content = View::forge('backend/superdir/resetuserpass',$data);
			}
			else{

				Session::set_flash('error','No user with that ID. please double check entry.');
			}
		}

		//reset user
		if(isset($_POST['resetPassword']))
		{
			//check if account exist;
			//checkUser ( 'userId')
			$result = Model_Admin::resetPassword($_POST['userid'] , $_POST['newpassword']);

			if ($result) {
				Session::set_flash('success','Successfully Changed User\'s Password.');				
			}
			else{

				Session::set_flash('error','Opps Something went wrong. Please re enter details.');
			}
		}
	}
	
	public function action_manageschedule(){
		$data['subjects'] = DB::select()->from('tbl_subjects')->execute()->as_array();
		$data['rooms'] = DB::select()->from('tbl_classroom')->execute()->as_array();
		$data['instructor'] = DB::select()->from('tbl_employees')->where('userType','faculty')->execute()->as_array();

		$data['schedule'] = $query = DB::select()->from('tbl_schedule')->execute()->as_array();

		$this->template->title = "TIE - Manage Schedule";
		$this->template->content = View::forge('backend/manageschedule', $data);
		
		if(isset($_POST['addSchedBtn'])){

			$val = Validation::forge('addSched');
			$val->add('subjectInstructor','Instructor')->add_rule('required');
			$val->add('subjectName','Subject')->add_rule('required');
			$val->add('subjectRoom','Classroom')->add_rule('required');
			$val->add('number-of-sched','Number of meetings')->add_rule('required');
			for($i = 0; $i <= ($_POST['number-of-sched']-1); $i++){
					$val->add('schedDay'.$i,'Select Day')->add_rule('required');
					$val->add('fromTime'.$i,'From Time')->add_rule('required');
					$val->add('toTime'.$i,'To Time')->add_rule('required');
				}
			//set custom message
			$val->set_message('required', ':label cannot be empty');
			

			//run validation
			if($val->run()){

				for($i = 0; $i <= ($_POST['number-of-sched']-1); $i++){

					$formData['userId'] = $_POST['subjectInstructor'];
					$formData['schedName'] = $_POST['subjectName'];
					$formData['schedRoom'] = $_POST['subjectRoom'];
					$formData['schedDay'] = $_POST['schedDay'.$i];
					$formData['schedTimeFrom'] = $_POST['fromTime'.$i];
					$formData['schedTimeTo'] = $_POST['toTime'.$i];

					// check if this time and room is available
					$availSched = $this->checkSchedAndRoom($formData);
					
					if(is_null($availSched)){
						$result = Model_Admin::saveSchedule($formData);	
					}
					else{
						Session::set_flash('error','Room and time not availabe. Choose a different schedule.');
						Response::redirect('backend/manageschedule');
					}
					
				}

				if ($result) {
					Session::set_flash('success','Successfully Added Schedule.');				
				}
				else{

					Session::set_flash('error','Opps Something went wrong. Please re enter details.');
				}
			}
			else{
				foreach ($val->error() as $key => $value) {
					Session::set_flash('error',$value->get_message());
				}
			}

		}
	}

	public function action_addcourse(){
		
		$data['courses'] = Model_Admin::getCourseList();

		$this->template->title = "TIE - Add Course";
		$this->template->content = View::forge('backend/superdir/addcourse' , $data);

		if(isset($_POST['addCourse']))
		{
			$val = Validation::forge('addAnnounce');
			$val->add('courseid','Course Id')->add_rule('required');
			$val->add('coursename','Course Name')->add_rule('required');
			//set custom message
			$val->set_message('required', ':label cannot be empty');
			

			//run validation
			if($val->run()){
				$result = Model_Admin::addCourse($_POST);

				if ($result) {
					Session::set_flash('success','Successfully Added Course '.$_POST['coursename']);				
				}
				else{

					Session::set_flash('error','Opps Something went wrong. Please re enter details.');
				}
			}
			else{
				foreach ($val->error() as $key => $value) {
					Session::set_flash('error',$value->get_message());
				}
			}
		}

		if(isset($_POST['addSubject']))
		{
			$val = Validation::forge('addAnnounce');
			$val->add('subjectId','Course Id')->add_rule('required');
			$val->add('subjectName','Course Name')->add_rule('required');
			$val->add('subjectUnits','Course Name')->add_rule('required');
			//set custom message
			$val->set_message('required', ':label cannot be empty');
			

			//run validation
			if($val->run()){
				$result = Model_Admin::addSubject($_POST);

				if ($result) {
					Session::set_flash('success','Successfully Added Subject');				
				}
				else{

					Session::set_flash('error','Opps Something went wrong. Please re enter details.');
				}
			}
			else{
				foreach ($val->error() as $key => $value) {
					Session::set_flash('error',$value->get_message());
				}
			}
		}
	}

	public function action_managecoursesubject(){
		// initialise subject variable on view
		$data['subjects'] = "";

		$this->template->title = 'TIE - Manage Course or Subjects';
		$this->template->content = View::forge('backend/superdir/managecoursesubject', $data);

		$query = DB::select('courseId' , 'courseName')->from('tbl_course')->execute()->as_array();
		
		if($query){
			$data['courses'] = $query;
			$this->template->content = View::forge('backend/superdir/managecoursesubject',$data);
		}
		
		if(isset($_POST['CourseList'])){
			
			$data['subjects'] = DB::select()->from('tbl_subjects')->where('courseId' , $_POST['CourseList'])->execute()->as_array();
			
			$this->template->content = View::forge('backend/superdir/managecoursesubject', $data);
		}
	}

	public function action_editsubjects(){
		//get subject details from query string
		$data['subjectDetail'] = DB::select()->from('tbl_subjects')->where('subjectId' , $_SERVER['QUERY_STRING'])->execute()->as_array();

		$this->template->title = 'TIE - Manage Course or Subjects';
		$this->template->content = View::forge('backend/superdir/editsubjects' , $data);

		if(isset($_POST['editSubject']))
		{
			$query = Model_Admin::updateSubjectDetail($_POST);

			if ($query) {
				Session::set_flash('success','Successfully Updated Subject');
				Response::redirect('backend/managecoursesubject');
			}
			else{

				Session::set_flash('error','Opps Something went wrong. No changes on the fields or wrong entries.');
				Response::redirect("backend/edit-subject?".$_POST['subjectId']);
			}
			
		}
	}

	public function action_addstudent(){
		$data['course'] = DB::select('courseId')->from('tbl_course')->execute()->as_array();

		$this->template->title = 'TIE - Admin Add Student';
		$this->template->content = View::forge('backend/admindir/addstudent', $data);

		if (isset($_POST['submitAccount']))
		{
			$query = Model_Admin::insertStudent($_POST
				);

			if ($query) {
				Session::set_flash('success','Successfully Added Student');
			}
			else{

				Session::set_flash('error','Opps Something went wrong. No changes on the fields or wrong entries.');
				Response::redirect("backend/add-student");
			}
		}
	}


	//backend  control for common admin

	// public function action_addsubjectsinstrc(){

	// 	$data['instructors'] = DB::select()->from('tbl_employees')->where('userType' , 'faculty')->execute()->as_array();
	// 	$data['subjects'] = DB::select()->from('tbl_subjects')->execute()->as_array();

	// 	$this->template->title = 'TIE - Manage Student Subject';
	// 	$this->template->content = View::forge('backend/admindir/addsubjectsinstrc' , $data);

	// 	if (isset($_POST['addSubIns'])) {
	// 		echo  implode($_POST['scheduleDay'],"|");
	// 	}
	// }

	public function action_managestudentsubject(){
		$this->template->title = 'TIE - Add Subject to instructor';
		$this->template->content = View::forge('backend/admindir/managestudentsubject');
	}

	//announcement and news
	public function action_announcement(){
		$this->template->title = 'TIE - Add Announcement';
		$this->template->content = View::forge('backend/announcement');

		if(isset($_POST['addAnnounce'])){

			//set validation rule
			$val = Validation::forge('addAnnounce');
			$val->add('newsTitle','Headline')->add_rule('required');
			$val->add('newsFeed','Content')->add_rule('min_length', 6)->add_rule('required');
			//set custom message
			$val->set_message('required', ':label cannot be empty');
			$val->set_message('min_length', ':label Must have a paragraph.');

			//run validation
			if($val->run()){
				$result = Model_Admin::addAnnounce($_POST);

				if($result){
					Session::set_flash('success','Successfully Spread the news.');
					Response::redirect('/backend/announcement');
				}
				else{
					Session::set_flash('error','Opps! Something went wrong.');
				}
			}
			else{
				foreach ($val->error() as $key => $value) {
					Session::set_flash('error',$value->get_message());
				}
			}
		}
	}

	public function action_news(){
		$this->template->title = 'TIE - Add News Feed';
		$this->template->content = View::forge('backend/news');

		if(isset($_POST['addNews'])){
			echo 'to';
			//set validation rule
			$val = Validation::forge('addAnnounce');
			$val->add('newsTitle','Headline')->add_rule('required');
			$val->add('newsFeed','Content')->add_rule('min_length', 6)->add_rule('required');
			//set custom message
			$val->set_message('required', ':label cannot be empty');
			$val->set_message('min_length', ':label Must have a sentence or paragraph.');

			//run validation
			if($val->run()){
				$result = Model_Admin::addNews($_POST);

				if($result){
					Session::set_flash('success','Successfully Spread the News.');
					Response::redirect('/backend/news');
				}
				else{
					Session::set_flash('error','Opps! Something went wrong.');
				}
			}
			else{
				foreach ($val->error() as $key => $value) {
					Session::set_flash('error',$value->get_message());
				}
			}
		}
	}

	public function action_manageannounnews(){
		$data['news'] = Model_Admin::getNews();
		
		$this->template->title = 'TIE - Manage News Feed';
		$this->template->content = View::forge('backend/manageannounnews' , $data);
	}

	//schedule and classroosms

	public function action_addrooms(){
		//tbl_classroom
		$this->template->title = 'TIE - Add classroom';
		$this->template->content = View::forge('backend/addrooms');

		if (isset($_POST['addRoom'])) {
			
			//set validation rule
			$val = Validation::forge('login');
			$val->add('roomName','Room Name')->add_rule('required');
			$val->add('roomCapacity','Room Capacitys')->add_rule('required');
			//set custom message
			$val->set_message('required', ':label cannot be empty');

			//run validation
			if($val->run()){
				$result = Model_Admin::addRoom($_POST);
				if($result){
					Session::set_flash('success','Successfully Saved Room '.$_POST['roomName'].'.');
					Response::redirect('/backend/addrooms');
				}
				else{
					Session::set_flash('error','Opps! Something went wrong.');
				}
			}
			else{
				//foreach ($val->error() as $key => $value) {
					Session::set_flash('error',$val->error());
				//}
			}



		}
	}

	private function checkSchedAndRoom($post){
		$query = DB::select()->from('tbl_schedule')->where('schedActive','true')->execute()->as_array();

		foreach ($query as $key => $value) {
			$oldFrom	= strtotime($value['schedTimeFrom']);
			$oldTo		= strtotime($value['schedTimeTo']);
			$oldRoom	= $value['schedRoom'];
			$oldDay		= $value['schedDay'];
			
			if($post['schedDay'] == $oldDay){
				if($post['schedRoom'] == $oldRoom){
					if (strtotime($post['schedTimeFrom']) >= $oldFrom && strtotime($post['schedTimeFrom']) <= $oldTo) {
						return false;
					}
				}
			}
		}
	}

}