<?php
$key = '!@#$%^&*';
class Util
{
	public static function encryptString($pure_string){
		global $key;
		$encrypted_string = Crypt::encode($pure_string,$key);
	    return $encrypted_string;
	}

	public static function  decryptString($encrypted_string) {
		global $key;
	    $decrypted_string = Crypt::decode($encrypted_string, $key);
	    return $decrypted_string;
	}

	public static function removeFormLast($post){
		array_pop($post);
		return $post;
	}
	
	//match password from database
	public static function checkUserCredentials($userData , $password){
		return ($userData['passWord'] == $password ? true : false);
	}

	//usage randomId(INT)
	public static function randomId($length){
		
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';

		for ($i = 0; $i < $length; $i++) {
		    $randomString .= $characters[rand(0, $charactersLength - 1)];
		}

	    return $randomString;
	}

	// usage checkIdExist ( string table_name , string id_column, string id )
	public static function checkIdExist($table , $columnId , $id){
		$result = DB::select()->from($table)->where( $columnId , $id)->execute();

		foreach($result as $key => $value){
			if($value[$columnId] === $id){
				return true;
			}
		}
	}

	public static function date(){
		return date("Y-m-d");
	}

	//a short config file to restrict access based on usertype
	public static function noAccess(){
		// list restricted pages
		$admin = array(
			'/home', '/home/',
			'/profile', '/profile/',
		);
		$student = array(
			'/backend',
			'/backend/home',
			'/backend/login',
		);

		//If there is no session or cookies place not allowed
		$noSess = array(
			'/home','/home/',
			'/profile', '/profile/', //student login
			'/backend/home', '/backend/home/', //admin and faculty login
			'/backend/manage-schedule' , '/backend/manage-schedule/',
			'/backend/manageschedule' , '/backend/manageschedule/'
		);

		//var_dump(Session::get()); exit();
		if(Session::get() || Session::get_flash()){  //with session
			if(Session::get('usertype') === 'student')
			{
				if(in_array($_SERVER['REQUEST_URI'],$student)) Response::redirect('home');
			}
			else
			{
				if(in_array($_SERVER['REQUEST_URI'],$admin)) Response::redirect('backend/home');
			}
		}
		else    //without session
		{
			//it redirects you to home page (index) if there is no session.
			if(in_array($_SERVER['REQUEST_URI'],$noSess)) Response::redirect('/'); 
		}
	}

	public static function loadAdminFormElements(){
		return Form::select('userType', 'Admin', array(
				'' => 'Select Role',
				'admin' => 'Admin',
				'faculty' => 'Faculty'));
	}


	//if array list present in haystack (required string , required array)
	private static function strposa($haystack, $needle){
	    
	    foreach($needle as $query)
	    {
	        if(strpos($haystack, $query) !== false) return true; // stop on first true result
	    }

	    return false;
	}
}
