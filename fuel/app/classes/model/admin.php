<?php
//use \DB;
class Model_Admin extends \Model
{

	public static function getUserPassword($username){

		$query = DB::query("SELECT `passWord`,`userId`,`userType` FROM `tbl_login` WHERE `userName` = '$username'")->execute();
		
		foreach ($query as $key => $value) {
			return $value;
		}

	}

	public static function getUserDetail($userId , $userType){

		$query = DB::query("SELECT * FROM `tbl_employees` WHERE `userId` = '$userId'")->execute();
			
		foreach ($query as $key => $value) {
			return $value;
		}
	}

	//checkUser ( 'userId' string)
	public static function checkUser($post){
		
		$query = DB::query("SELECT * FROM `tbl_login` WHERE `userId` = '$post'")->execute();

		foreach ($query as $key => $value) {
			return $value;
		}
	}

	//resetPassword('userId' string , 'newTempPass' string)
	public static function resetPassword($userId , $newTempPass){
		$date = date("Y-m-d");

		$result = DB::query("UPDATE tbl_login SET passWord = '$newTempPass', updated_at = '$date' WHERE  userId = '$userId'")->execute();
		
		return $result;
		

	}

	public static function addCourse($post){
		$date = date("Y-m-d");

		$result = DB::insert('tbl_course')->set(array(
								'courseId' => $post['courseid'],
								'courseName' => $post['coursename'],
								'created_at' => $date,
			))->execute();

		foreach ($result as $key => $value) {
			return $value;
		}
	}

	public static function addRoom($post){
		//generate random ID
		$roomId = 'RM-'.Util::randomId(5);
		
		//check if ID already Exist//
		$idCheck = Util::checkIdExist('tbl_classroom' , 'roomId' , $roomId);
			
		while ($idCheck) {
			$idCheck = Util::checkIdExist('tbl_classroom' , 'roomId' , $roomId);
			
			$roomId = 'RM-'.Util::randomId(5);
			
		}//end check//


		$result = DB::insert('tbl_classroom')->set(array(
							'roomId' => $roomId,
							'roomName' => $post['roomName'],
							'roomCapacity' => $post['roomCapacity'],
							'created_at' => Util::date(),
		))->execute();

		foreach ($result as $key => $value) {
			return $value;
		}
	}

	public static function addNews($post){
		//generate random ID
		$Id = 'NW-'.Util::randomId(5);
		
		//check if ID already Exist//
		$idCheck = Util::checkIdExist('tbl_news_feed' , 'newsId' , $Id);
			
		while ($idCheck) {
			$idCheck = Util::checkIdExist('tbl_news_feed' , 'newsId' , $Id);
			
			$Id = 'NW-'.Util::randomId(5);
			
		}//end check//

		$query = DB::insert('tbl_news_feed')->set(array(
										'newsId' => $Id,
										'newsTitle' => $post['newsTitle'],
										'newsFeed' => $post['newsFeed'],
										'status' => 'active',
										'created_at' => Util::date(),
										))->execute();

		foreach ($query as $key => $value) {
			return $value;
		}

	}

	public static function getNews(){
		return DB::select()->from('tbl_news_feed')->execute()->as_array();
	}

	public static function saveSchedule($post){
		$schedInstructor = $post['schedInstructor'];
		$schedName = $post['schedName'];
		$schedRoom = $post['schedRoom'];
		$schedDay = $post['schedDay'];
		$schedTimeFrom = $post['schedTimeFrom'];
		$schedTimeTo = $post['schedTimeTo'];

		//generate random ID
		$Id = 'SC-'.Util::randomId(5);
		
		//check if ID already Exist//
		$idCheck = Util::checkIdExist('tbl_schedule' , 'schedId' , $Id);
			
		while ($idCheck) {
			$idCheck = Util::checkIdExist('tbl_schedule' , 'schedId' , $Id);
			
			$Id = 'SC-'.Util::randomId(5);
			
		}//end check//


		//store to database...
		$query = DB::insert('tbl_schedule')->set(array(
							'schedId' => $Id,
							'userId' => $userId,
							'schedName' => $schedName,
							'schedRoom' => $schedRoom,
							'schedDay' => $schedDay,
							'schedTimeFrom' => $schedTimeFrom,
							'schedTimeTo' => $schedTimeTo,
							'schedActive' => 'true',
							'created_at' => UTil::date(),
			))->execute();

		foreach ($query as $key => $value) {
			return $value;
		}
	}

	public static function getCourseList(){
		return $result = DB::select()->from('tbl_course')->execute()->as_array();
	}

	public static function addSubject($post){
		$subjectId = $post['subjectId'];
		$courseId = $post['courseId'];
		$subjectName = $post['subjectName'];
		$subjectUnits = $post['subjectUnits'];
		$date = date("Y-m-d");
		
		$query = DB::insert('tbl_subjects')->set(array(
										'subjectId' => $subjectId,
										'courseId' => $courseId,
										'subjectName' => $subjectName,
										'subjectUnits' => $subjectUnits,
										'created_at' => $date,
										))->execute();

		foreach ($query as $key => $value) {
			return $value;
		}
	}

	public static function insertAdminOrStaff($post){
		$date = date("Y-m-d");
		$result = DB::insert('tbl_login')->set(array(
								'userId' => $post['employeeId'],
								'userType' => $post['userType'],
								'userName' => $post['userName'],
								'passWord' => $post['tempPass'],
								'isActive' => 'active',
								'created_at' => $date,
							))->execute();
		$result = DB::insert('tbl_employees')->set(array(
								'userId' => $post['employeeId'],
								'userType' => $post['userType'],
								'firstName' => $post['firstName'],
								'lastName' => $post['lastName'],
								'emailAddress' => $post['emailAddress'],
								'departmentId' => $post['departmentId'],
								'created_at' => $date,
							))->execute();
		return $result[1];
	}

	public static function updateSubjectDetail($post){
		$date = date("Y-m-d");
		$query = DB::Update('tbl_subjects')->set(array(
									'subjectId' => $post['subjectId'],
									'subjectName' => $post['subjectName'],
									'subjectUnits' => $post['subjectUnits'],
									'updated_at' => $date,

								))->where('subjectId' , $post['subjectId'])->execute();
		return $query;
	}

	public static function insertStudent($post){
		$date = date("Y-m-d");
		$result = DB::insert('tbl_login')->set(array(
								'userId' => $post['studentId'],
								'userType' => $post['usertype'],
								'userName' => $post['userName'],
								'passWord' => $post['tempPass'],
								'isActive' => 'active',
								'created_at' => $date,
							))->execute();
		$result = DB::insert('tbl_students')->set(array(
								'studentId' => $post['studentId'],
								'usertype' => $post['usertype'],
								'firstName' => $post['firstName'],
								'lastName' => $post['lastName'],
								'emailAddress' => $post['emailAddress'],
								'courseId' => $post['courseId'],
								'created_at' => $date,
							))->execute();
		return $result[1];
	}
}