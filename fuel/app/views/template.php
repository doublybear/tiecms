<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo $title; ?></title>
	<link rel="shortcut icon" href="favicon.ico">
	<?php echo Asset::css(array('app.css','style.css','font-awesome.css')); ?>
		
	<?php echo Asset::js('app.js'); ?>
	<?php echo Asset::js('../bower_components/foundation/js/vendor/modernizr.js'); ?>
	<?php echo Asset::js('../bower_components/foundation/js/vendor/jquery.js'); ?>
</head>
<body>
		<header class="contain-to-grid sticky">
			<nav class="top-bar" data-topbar role="navigation">
			<?php //check if session is available and display proper menu ?>
			<?php if(null !== Session::get('userid')): ?>
				<?php //compare if user is student or employee and display appropriate menu ?>
				<?php if(Session::get('usertype') == 'admin' || Session::get('usertype') == 'faculty'): ?>
					<ul class="title-area">
						<li class="name">
							<?php echo Html::anchor('backend/home',Html::img('assets/img/tie-logo.png'));?>
						</li>

					</ul>
					<section class="top-bar-section">
						<ul class="right">
								<li><?php echo Html::anchor('backend/home','Home') ?></li>
								<li><?php echo Html::anchor('backend/logout','LOG-OUT') ?></li>
						</ul>
					</section>
				<?php else: ?>
					<ul class="title-area">
						<li class="name">
							<?php echo Html::anchor('home',Html::img('assets/img/tie-logo.png'));?>
						</li>
					</ul>
					<section class="top-bar-section">
						<ul class="right">
								<li><?php echo Html::anchor('home','Home') ?></li>
								<li><?php echo Html::anchor('profile','Announcements') ?></li>
								<li><?php echo Html::anchor('profile','Grades') ?></li>
								<li><?php echo Html::anchor('/home/logout','LOG-OUT') ?></li>
						</ul>
					</section>
				<?php endif; ?>

			<?php else: ?>
				<?php if((strpos($_SERVER['REQUEST_URI'],'/backend') !== false)): ?>				
					<ul class="small-6 large-centered columns">
						<li class="name text-center">
							<?php echo Html::anchor('/backend',Html::img('assets/img/tie-logo.png'));?>
						</li>

					</ul>
				<?php else: ?>
					<ul class="title-area">
						<li class="name">
							<?php echo Html::anchor('/',Html::img('assets/img/tie-logo.png'));?>
						</li>

					</ul>
					<section class="top-bar-section">
						<ul class="right">
								<li><?php echo Html::anchor('login','LOG-IN') ?></li>
						</ul>
					</section>
				<?php endif; ?>
				
			<?php endif; ?>
			</nav>
		</header>

		<div class="row">
			<div class="small-12 large-6 large-centered columns">
				<?php if (Session::get_flash('success')): ?>
					<div data-alert class="alert-user alert-box success radius">
						<strong>Success</strong>
						<p>
						<?php echo implode('</p><p>', e((array) Session::get_flash('success'))); ?>
						</p>
						<a href="#" class="close">&times;</a>
					</div>
				<?php endif; ?>
				<?php if (Session::get_flash('error')): ?>
					<div data-alert class="alert-user alert-box alert radius">
						<strong>Error</strong>
						<p>
						<?php foreach (Session::get_flash('error') as $key => $value): ?>
							<?php echo $value.'<br>' ?>
						<?php endforeach ?>
						</p>
						<a href="#" class="close">&times;</a>
					</div>
				<?php endif; ?>
			</div>
		</div>

		<div class="content">
			<?php echo $content; ?>
		</div>
		<div class="spacer"></div>
		<footer>
			<div class="row">
				<div class="small-6 large-centered text-center columns">
						<p>
							<?php if (!Session::get()): ?>
								<?php echo Html::anchor('/backend','Login') ?>	
							<?php endif ?>							
						<br>
							TIE CMS &copy; <?= date('Y') ?>
						</p>
				</div>
			</div>
		</footer>
		<?php echo Asset::js('../bower_components/foundation/js/vendor/jquery.js'); ?>
		<?php echo Asset::js('../bower_components/foundation/js/vendor/fastclick.js'); ?>
		<?php echo Asset::js('../bower_components/foundation/js/foundation.min.js'); ?>
</body>
</html>
<script src="assets/bower_components/foundation/js/foundation.min.js"></script>
<script>
	$(document).foundation();
</script>