<div class="spacer"></div>

<div class="row">
	<div class="large-8 small-12 columns">
		<div class="row">
			<div class="large-12 small-12 columns">
				<h1>
					Welcome <b><?php echo $userdetail['firstName'] ?></b>!
				</h1>		
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="large-12 small12 columns">
				<p>Click on a subject below to get started.</p>
				<p>If you do not see any subjects listed, it means either:</p>
				<ol>
					<li>Your subject/s is not quite ready yet.</li>
					<li>You may not be enrolled in the class. if you suspect this is the case, please see your instructor to address the issue.</li>
				</ol>
				<p>If you run into any technical problems, inform your instructor and or the IT admin department.</p>
			</div>
		</div>
		
		<div class="row">
			<div class="large-12 small12 columns">
				<h2>Announcements</h2>
				<hr>
				<h3>- Subject 1 - Mr. Stark -</h3>
				<p>Final grading is near. please present your final project/s asap.</p>
				<hr>
				<h3>- Subject 1 - Mr. Stark -</h3>
				<p>Study Soldering 101. Will appear on your final exam.</p>

				<br>
				<a href="">see all announcements</a>
			</div>
		</div>
		<div class="spacer"></div>
		<div class="row">
			<h2>Your Current Subject/s</h2>
			<hr>
			<div class="large-12 small12 columns">
				<?php //dynamically get year today and retrieve from db which correspond with the conditiona ?>
				<h3>S.Y. 2014 - 2015</h3>
				<table>
					<tr>
						<th>Subject Name</th>
						<th>Subject Schedule</th>
						<th>Subject Room</th>
					</tr>
					<?php //get data from database. start foreach here ?>
					<tr>
						<td>Internet Technology</td>
						<td></td>
						<td></td>
					</tr>	
				</table>
			</div>
		</div>

		<div class="spacer"></div>		
	</div>

	<div class="large-4 small-12 columns">
		<div class="row  supmenu">
			<div class="large-12 small-12 columns">
				<h2>Widget Menu</h2>
				<hr>
				<br>
				<ul class="no-bullet su-button">
					<li>
						<h4>News Bulletin</h4>
						<hr>
						<?php $i = 0; ?>
						<?php foreach ($news as $key => $value): ?>			
							<h2><?php echo $value['newsTitle'] ?></h2>
							<p><?php echo $value['newsFeed'] ?></p>
							<?php $i++; ?>
							<?php if ($i==2) break;?>
					<?php endforeach ?>
						<p><a href="/home/new">View All News..</a></p>
						<br>
						<hr>
					</li>
					<li>
						<a href="add-student">View your grades</a>				
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>