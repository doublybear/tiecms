<div class="spacer"></div>

<div class="row supermenu">
	<div class="large-12 small-12 large-centered">
		<h2 >Latest School News</h2>
		<hr>
		<br>
		<div class="row">
			<div class="large-11 large-centered small-12 columns">
				<?php $i = 0; ?>
				<?php foreach ($news as $key => $value): ?>			
					<h2><?php echo $value['newsTitle'] ?></h2>
					<p><?php echo $value['newsFeed'] ?></p>
					<?php $i++; ?>
					<?php if ($i==2) break;?>
				<?php endforeach ?>
				<p><a href="/home/new">View All News..</a></p>
			</div>
		</div>
	</div>
</div>

<div class="spacer"></div>

<div id="banner">
	<div class="row">
		<div class="large-12 middle-12 columns text-center call-to-action">
			<h1>Course Management System</h1>
		</div>
	</div>
	<div class="row">
		<div class="large-12 middle-12 columns text-center call-to-action">
			<h3>Mission</h3>
			<p style="color:#ffffff">To create and disseminate knowledge. TIE aspires to generate leading-edge research with global impact as well as to produce broad-based, creative and entrepreneurial leaders for the knowledge-based economy. TIE is committed to an interactive, participative and technologically enabled learning experience. Towards this end, it will provide a rewarding and challenging environment for faculty, staff and students to kindle and sustain a passion for excellence. </p>
		</div>
	</div>
	<div class="row">
		<div class="large-12 middle-12 columns text-center call-to-action">
			<h3>Vision</h3>
			<p style="color:#ffffff">To be a great and iconic learning institution in Leyte that excels in tackling the world’s complexities and impacting humanity positively and producing leaders of tomorrow through its transformative education and multi-disciplinary research to provide insights in solving these problems.</p>
		</div>
	</div>
</div>

<div class="spacer"></div>