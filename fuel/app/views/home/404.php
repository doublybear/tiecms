<div id="four-o-four-header"></div>
<div class="spacer"></div>
<div class="row" id="blog">
	<div class="large-12 colums">
		<h1 class="text-center"> Error 404 </h1>
		<h2 class="text-center">Opps! Looks Like We Messed Up or This page doesn't exist.</h2>
		<?php if(!Session::get()): ?>
			<p class="text-center">Not to worry. You can head back to our <?php echo Html::anchor('/','Homepage') ?>.</p>
		<?php elseif(Session::get('usertype') === 'student'): ?>
			<p class="text-center">Not to worry. You can head back to our <?php echo Html::anchor('/home','Homepage') ?>.</p>
		<?php else: ?>
			<p class="text-center">Not to worry. You can head back to our <?php echo Html::anchor('/backend/home','Homepage') ?>.</p>
		<?php endif; ?>

	</div>
</div>