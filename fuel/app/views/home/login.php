<div class="spacer"></div>
<div class="row">
	<div class="rounded-border small-12 large-6 large-centered columns">
		
		<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'class' => 'login-form')) ?>
			<?php echo Form::input('loginUsername' , '' ,array('type' => 'text' , 'placeholder' => 'Enter Username')) ?>
			<?php echo Form::input('loginPassword', '' ,array('type' => 'password' , 'placeholder' => 'Enter Password')) ?>
			<?php echo Form::button(array('name' => 'submitLogin','value' => 'Login', 'type' => 'submit','class' => 'button small'))?>
		<?php echo Form::close() ?>
	</div>
</div>