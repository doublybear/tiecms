<div class="spacer"></div>
<div class="row">
	<div class="large-12 small-12 columns">
		<h2>School Bulletin News Section</h2>
		<hr>
		<br>
		<div class="large-10 large-centered columns">
			<?php foreach ($news as $key => $value): ?>
				<br>
				<h2><?php echo $value['newsTitle'] ?></h2>
				
				<p><?php echo $value['newsFeed'] ?></p>

				<hr>
			<?php endforeach ?>
		</div>	
	</div>
	
</div>