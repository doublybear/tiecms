<div class="spacer"></div>
<div class="row">
	<div class="large-8 small-12 large-centered columns">
	<h2>Add Schedule to subject</h2>
		<hr>
		<br>
		<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" id="addSched">
			<label for="subjectInstructor">Choose Instructor:</label>
			<select name="subjectInstructor" id="subjectInstructor">
				<option value="">Select Instructor</option>
				<?php foreach ($instructor as $key => $value): ?>
					<option value="<?php echo $value['userId'] ?>">
						<?php echo $value['firstName']." ".$value['lastName'] ?>	
					</option>		
				<?php endforeach ?>
			</select>
			<label for="subjectName">Choose Subject:</label>
			<select name="subjectName" id="subjectName">
				<option value="">Select Subject</option>
				<?php foreach ($subjects as $key => $value): ?>
					<option value="<?php echo $value['subjectName'] ?>">
						<?php echo $value['subjectName'] ?>	
					</option>		
				<?php endforeach ?>
			</select>
			<label for="subjectRoom">Choose Room:</label>
			<select name="subjectRoom" id="subjectRoom">
				<option value="">Select Classroom</option>
				<?php foreach ($rooms as $key => $value): ?>
					<option value="<?php echo $value['roomId'] ?>">
						<?php echo $value['roomName'] ?>	
					</option>		
				<?php endforeach ?>
			</select>

			<label for="schedSY">School Year and Semester</label>
			<div class="row">
				<div class="large-8 columns">
					<select name="schedSY2" id="schedSY2">
						<?php for ($i=2014; $i <= 2100; $i++): ?>
							<option value="<?php echo $i.' - '.($i+1) ?>"><?php echo $i.' - '.($i+1) ?></option>
						<?php endfor; ?>
					</select>
				</div>
				<div class="large-4 columns">
					<select name="schedSem" id="schedSem">
						<option value="1st">1st</option>
						<option value="2nd">2nd</option>
						<option value="3rd">3rd</option>
					</select>
				</div>
			</div>
			
			How Many Meetings?
			<select class="number-of-sched" name="number-of-sched">
				<option value="">Select Number of Schedule</option>
				<?php for($i=1 ; $i <= 7; $i++): ?>
					<option value="<?php echo $i ?>">
						<?php echo $i ?>	
					</option>
				<?php endfor; ?>
			</select>
			<div id="addedForm"></div>
			<input type="submit" value="Save" name="addSchedBtn" class="small button right">
		</form>
	</div>
</div>
<div class="row">
	<div class="large-12 small-12 large-centered columns">
	<h2>Edit Active Schedules</h2>
		<hr>
		<br>
		<table style="width:100%">
			<tr>
				<th>Instructor</th>
				<th>Subject</th>
				<th>Room</th>
				<th>Schedule</th>
				<th></th>
				<th></th>
			</tr>
			<?php if (isset($schedule)): ?>
				<?php foreach ($schedule as $key => $value): ?>
					<?php if ($value['schedActive'] == 'true'): ?>
					<tr>
						<?php //tbl_employees from userId in both tables ?>
						<?php foreach ($instructor as $keyI => $valueI): ?>
							<?php if ($value['userId'] == $valueI['userId']): ?>
								<td><?php echo $valueI['firstName']." ".$valueI['lastName'] ?>		
							<?php endif ?>
						<?php endforeach ?>
						
						<td><?php echo $value['schedName'] ?></td>
						
						<?php //tbl_classrooms from roomId in both tables ?>
						<?php foreach ($rooms as $keyR => $valueR): ?>
							<?php if ($value['schedRoom'] == $valueR['roomId']): ?>
								<td><?php echo $valueR['roomName'] ?></td>
							<?php endif ?>
						<?php endforeach ?>
						<td><?php echo $value['schedDay'] ?></td>
						<td><?php echo $value['schedTimeTo'].' - '.$value['schedTimeFrom']  ?></td>
						<td><a href="">edit</a></td>
						<td><a href="">delete</a></td>
					</tr>	
					<?php endif ?>
			
				<?php endforeach ?>
			<?php endif ?>
			
		</table>
	</div>
</div>

<div class="row">
	<div class="large-12 small-12 large-centered columns">
	<h2>View In-Active Schedules</h2>
		<hr>
		<br>
		<table style="width:100%">
			<tr>
				<th>Instructor</th>
				<th>Subject</th>
				<th>Room</th>
				<th>Schedule</th>
			</tr>
			<?php if (isset($schedule)): ?>
				<?php foreach ($schedule as $key => $value): ?>
					<?php if ($value['schedActive'] == 'false'): ?>
					<tr>
						<?php //tbl_employees from userId in both tables ?>
						<?php foreach ($instructor as $keyI => $valueI): ?>
							<?php if ($value['userId'] == $valueI['userId']): ?>
								<td><?php echo $valueI['firstName']." ".$valueI['lastName'] ?>		
							<?php endif ?>
						<?php endforeach ?>
						
						<td><?php echo $value['schedName'] ?></td>
						
						<?php //tbl_classrooms from roomId in both tables ?>
						<?php foreach ($rooms as $keyR => $valueR): ?>
							<?php if ($value['schedRoom'] == $valueR['roomId']): ?>
								<td><?php echo $valueR['roomName'] ?></td>
							<?php endif ?>
						<?php endforeach ?>
						<td><?php echo $value['schedDay'] ?></td>
						<td><?php echo $value['schedTimeTo'].' - '.$value['schedTimeFrom']  ?></td>
					</tr>	
					<?php endif ?>
			
				<?php endforeach ?>
			<?php endif ?>
			
		</table>
	</div>
</div>


<?php// '<input type="time" name="toTime" data-format="HH:mm" data-template="HH : mm" value="12:23">' ?>

<script>
	$("select.number-of-sched").on("change", function () {
    var number = parseInt($(".number-of-sched").val());
    var newDropList = '<option value="Mon">Monday</option><option value="Tue">Tuesday</option><option value="Wed">Wednesday</option><option value="Thur">Thursday</option><option value="Fri">Friday</option><option value="Sat">Saturday</option><option value="Sun">Sunday</option></select>';
    var html = '';

    $("#addedForm").html('');
    
    for (i = 0; i < number; i++) {
    	html += '<div class="row">';
	    	html += '<div class="large-6 small-12 columns">';
		        html += '<label>Select Day: </label>';
		        html += '<select name="schedDay'+i+'">'+newDropList;
	        html += '</div>';
	        html += '<div class="large-3 small-6 columns">';
		        html += '<label>From: </label>';
		        html += '<input type="time" name="fromTime'+i+'"'+' data-format="HH:mm" data-template="HH : mm">';
	        html += '</div>';
	        html += '<div class="large-3 small-6 columns">';
		        html += '<label>To: </label>';
		        html += '<input type="time" name="toTime'+i+'"'+' data-format="HH:mm" data-template="HH : mm">';
	        html += '</div>';
        html += '</div>';
    };

    $("#addedForm").html(html);
});
</script>