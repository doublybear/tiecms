<div class="spacer"></div>
<div class="row">
	<div class="small-12 large-12">
		<div class="small-6 large-centered columns">
			<h3 class="text-center">List of Courses</h3>
			<hr>
			<br>
			<?php if (isset($courses)): ?>
			<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'id' => 'getCourseList')) ?>
			<select name="CourseList" id="courseList">
					<option value="">Select Course</option>
					<?php foreach ($courses as $key => $value): ?>
						<option value="<?php echo $value['courseId'] ?>"><?php echo $value['courseName'] ?></option>
					<?php endforeach ?>
			</select>
			<?php echo Form::close() ?>
			<?php endif ?>

			<?php if ($subjects): ?>
				<table>
					<?php foreach ($subjects as $key): ?>
						<tr>
							<td><?php echo $key['subjectName'] ?></td>
							<td><?php echo Html::anchor('backend/edit-subject?'.$key['subjectId'] , 'Edit', array('class' => 'small button')); ?></td>
						</tr>
					<?php endforeach ?>
				</table>
			<?php elseif($subjects === ""): ?>
				<?php //do nothing ?>
			<?php else: ?>
				<h3>No Subject/s Available</h3>
				<hr>
				<br>
				<?php echo Html::anchor('backend/addCourse', 'Add Subject Now?' , array('class' => 'small button')) ?>
			<?php endif ?>
		</div>
	</div>
</div>

<script>
	jQuery(document).ready(function($) {

		$(document).ready(function(){
			$("#courseList").on("change",function(){
				$("#getCourseList").submit();
			});
		});
	});
</script>