<div class="spacer"></div>
<div class="row">
	<div class="small-12 large-12">
		<div class="small-6 large-centered columns">
			<h3 class="text-center">Reset Password</h3>
			<hr>
			<br>
			<?php if (isset($result)): ?>
				<h3>Give <?php echo ucfirst($result['userName']) ?> a temporary password</h3>
				<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'class' => 'login-form')) ?>
					<?php echo Form::input('userid' , $result['userId'] ,array('type' => 'hidden')) ?>
					<?php echo Form::input('newpassword' , '' ,array('type' => 'text' , 'placeholder' => 'new Temp Password')) ?>
					<?php echo Form::button(array('name' => 'resetPassword','value' => 'Change Password', 'type' => 'submit','class' => 'button small'))?>
				<?php echo Form::close() ?>
			<?php else: ?>
				<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'class' => 'login-form')) ?>
					<?php echo Form::input('loginuserid' , '' ,array('type' => 'text' , 'placeholder' => 'Enter Employee/Student Id')) ?>
					<?php echo Form::button(array('name' => 'checkUser','value' => 'Search User', 'type' => 'submit','class' => 'button small'))?>
				<?php echo Form::close() ?>
			<?php endif ?>
		</div>
	</div>
</div>
