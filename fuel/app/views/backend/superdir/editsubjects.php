<div class="spacer"></div>
<div class="row" id="editSubject">
	<div class="large-9 small-9 small-centered columns">
		<?php if (isset($subjectDetail)): ?>
			<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', )) ?>
				<table>
				  <tbody>
				    <tr>
				      <td width="200" class="text-right">
				      	<?php echo Form::label('Subject Id:','subjectId') ?>
				      </td>
				      <td width="600">
				      	<?php echo Form::input('subjectId' , $subjectDetail[0]['subjectId'] ,array('type' => 'text')) ?>
				      </td>
				    </tr>
				    <tr>
				      <td width="200" class="text-right">
				      	<?php echo Form::label('Subject Name:','subjectName') ?>
				      </td>
				      <td width="600">
				      	<?php echo Form::input('subjectName' , $subjectDetail[0]['subjectName'] ,array('type' => 'text')) ?>
				      </td>
				    </tr>
				    <tr>
				      <td width="200" class="text-right">
				      	<?php echo Form::label('Subject Units:','subjectUnits') ?>
				      </td>
				      <td width="600">
				      	<?php echo Form::input('subjectUnits' , $subjectDetail[0]['subjectUnits'] ,array('type' => 'text')) ?>
				      </td>
				    </tr>
				    <tr>
				    	<td>
				    	</td>
				    	<td class="text-right">
				    		<span class="button small" id="cancelEditSubject">Cancel</span>
				    		<?php echo Form::button(array('name' => 'editSubject','value' => 'Update', 'type' => 'submit' , 'class' => 'button small'))?>
				    	</td>
				    </tr>
				  </tbody>
				</table>
			<?php echo Form::close() ?>
		<?php endif ?>
	</div>
</div>