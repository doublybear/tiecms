<div class="spacer"></div>
<div class="row">
	<div class="small-6 large-centered columns">
		<h3 class="text-center">Add Course</h3>
		<hr>
		<br>
		<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'class' => 'login-form')) ?>
			<?php echo Form::input('courseid' , '' ,array('type' => 'text' , 'placeholder' => 'Enter Course Id')) ?>
			<?php echo Form::input('coursename' , '' ,array('type' => 'text' , 'placeholder' => 'Enter Course Name')) ?>
			<?php echo Form::button(array('name' => 'addCourse','value' => 'Add Course', 'type' => 'submit','class' => 'button small'))?>
		<?php echo Form::close() ?>
	</div>
</div>
<div class="spacer"></div>
<div class="row">
	<div class="small-6 large-centered columns">
		<h3 class="text-center">Add Subject</h3>
		<hr>
		<br>
		<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'class' => 'login-form')) ?>
			<select name="courseId" id="courseId">
				<?php foreach ($courses as $key => $value): ?>
					<option value="<?php echo $value['courseId']  ?>"><?php echo $value['courseId']." - ".$value['courseName']  ?></option>		
				<?php endforeach ?>	
			</select>
			<?php echo Form::input('subjectId' , '' ,array('type' => 'text' , 'placeholder' => 'Enter Subject Id')) ?>
			<?php echo Form::input('subjectName' , '' ,array('type' => 'text' , 'placeholder' => 'Enter Subject Name')) ?>
			<?php echo Form::input('subjectUnits' , '' ,array('type' => 'text' , 'placeholder' => 'Enter Number of Units')) ?>
			<?php echo Form::button(array('name' => 'addSubject','value' => 'Search User', 'type' => 'submit','class' => 'button small')) ?>
		<?php echo Form::close() ?>
	</div>
</div>
