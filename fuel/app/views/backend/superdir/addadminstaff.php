<div class="spacer"></div>
<div class="row">
	<div class="rounded-border small-12 large-6 large-centered columns">
		<h3>Add an Admin Account.</h3>
		<hr>
		<br>
		<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'class' => 'login-form')) ?>
			<?php echo Form::label('Employee Id:','userId') ?>
			<?php echo Form::input('employeeId' , '' ,array('type' => 'text')) ?>

			<?php echo Form::label('User Type:','userType') ?>
			<?php //get select form from util class. cannot include code here too long. using util class to aid coding ?>
			<?php echo Util::loadAdminFormElements() ?>


			<?php echo Form::label('Username:','userName') ?>
			<?php echo Form::input('userName' , '' ,array('type' => 'text')) ?>

			<?php echo Form::label('Temp. Password:','tempPass') ?>
			<?php echo Form::input('tempPass' , '' ,array('type' => 'text')) ?>

			<?php echo Form::label('First Name:','firstName') ?>
			<?php echo Form::input('firstName' , '' ,array('type' => 'text')) ?>

			<?php echo Form::label('Last Name:','lastName') ?>
			<?php echo Form::input('lastName' , '' ,array('type' => 'text')) ?>

			
			<?php echo Form::label('Department Name:','departmentId') ?>
			<?php echo Form::input('departmentId' , '' ,array('type' => 'text')) ?>

			<?php echo Form::label('Email Address:','emailAddress') ?>
			<?php echo Form::input('emailAddress' , '' ,array('type' => 'text')) ?>

			<?php echo Form::button(array('value' => 'Cancel', 'type' => 'Reset' , 'class' => 'button small'))?>
			<?php echo Form::button(array('name' => 'submitAccount','value' => 'Save', 'type' => 'submit' , 'class' => 'button small'))?>
		<?php echo Form::close() ?>
	</div>
			
</div>


