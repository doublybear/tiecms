<?php if (Session::get('usertype') === 'admin'): ?>
	<div data alert class="alert-box text-center">This is an admin account! Any changes here are sensitive. Use caution.</div>
<?php endif ?>
<div class="spacer"></div>
<div class="row">
	<div class="large-12 columns">
		<h1>
			Welcome <b><?php echo ucfirst($userdetail['firstName']) ?></b>!
		</h1>
	</div>
</div>
<div class="row">
		<?php echo $usertypepage; ?>
</div>