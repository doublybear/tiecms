<div class="spacer"></div>
<div class="row">
	<div class="rounded-border small-12 large-6 large-centered columns">
		<h3>Add an Student Account.</h3>
		<hr>
		<br>
		<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'class' => 'login-form')) ?>
			<?php echo Form::label('Student Id:','studentId') ?>
			<?php echo Form::input('studentId' , '' ,array('type' => 'text')) ?>
			
			<?php echo Form::input('usertype' , 'student' ,array('type' => 'hidden', 'readonly' => 'readonly')) ?>

			<?php echo Form::label('Username:','userName') ?>
			<?php echo Form::input('userName' , '' ,array('type' => 'text')) ?>

			<?php echo Form::label('Password:','tempPass') ?>
			<?php echo Form::input('tempPass' , '' ,array('type' => 'text')) ?>

			<?php echo Form::label('First Name:','firstName') ?>
			<?php echo Form::input('firstName' , '' ,array('type' => 'text')) ?>

			<?php echo Form::label('Last Name:','lastName') ?>
			<?php echo Form::input('lastName' , '' ,array('type' => 'text')) ?>

			
			<?php echo Form::label('Course:','courseId') ?>
			<select name="courseId" id="courseId">
				<?php foreach ($course as $key): ?>
					<option value="<?php echo $key['courseId']?>"> <?php echo $key['courseId']?> </option>
				<?php endforeach ?>
			</select>

			<?php echo Form::label('Email Address:','emailAddress') ?>
			<?php echo Form::input('emailAddress' , '' ,array('type' => 'text')) ?>

			<?php echo Form::button(array('value' => 'Cancel', 'type' => 'Reset' , 'class' => 'button small'))?>
			<?php echo Form::button(array('name' => 'submitAccount','value' => 'Update', 'type' => 'submit' , 'class' => 'button small'))?>
		<?php echo Form::close() ?>
	</div>
			
</div>


