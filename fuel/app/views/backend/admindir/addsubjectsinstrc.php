<div class="spacer"></div>
<div class="row">
	<div class="large-12 small-12 columns">
		<div class="large-6 small-10 large-centered small-centered columns supmenu">
			<h3>Add Subject to this instructor</h3>
			<table>
			<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'id' => 'getCourseList')) ?>
				<tr>
					<td>
						<label for="instructorList">Select Instructor: </label>
					</td>
					<td>
						<select name="instructorList" id="instructorList">
							<option value="">Select Instructor</option>
							<?php foreach ($instructors as $key => $value): ?>
								<option value="<?php echo $value['userId'] ?>"><?php echo ucfirst($value['firstName'])." ".ucfirst($value['lastName']) ?></option>
							<?php endforeach ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="subjectList">Select Subject: </label>
					</td>
					<td>
						<select name="subjectList" id="subjectList">
								<option value="">Select Subject</option>
								<?php foreach ($subjects as $key => $value): ?>
									<option value="<?php echo $value['subjectId'] ?>"><?php echo ucfirst($value['subjectName'])?></option>
								<?php endforeach ?>
						</select>
					</td>
				<tr>
					<td>
						<label for="schedule">Place Schedule: </label>
					</td>
					<td>
						M&nbsp;<input name="scheduleDay[]" type="checkbox" value="M"> &nbsp; T&nbsp;<input name="scheduleDay[]" type="checkbox" value="T"> &nbsp;
						W&nbsp;<input name="scheduleDay[]" type="checkbox" value="W"> &nbsp; Th&nbsp;<input name="scheduleDay[]" type="checkbox" value="TH"> &nbsp;
						F&nbsp;<input name="scheduleDay[]" type="checkbox" value="F"> &nbsp; S&nbsp;<input name="scheduleDay[]" type="checkbox" value="S"> &nbsp;
						<input name="scheduleTime" type="text" placeholder="TIME">
						<input name="scheduleRoom" type="text" placeholder="ROOM ">
					</td>
				</tr>
				<tr>
					<td>
						<label for="capacity">Student Capacity: </label>
					</td>
					<td>
						<select name="capacity" id="capacity">
							<option value="">Select number of students</option>
							<?php for ($i=1; $i <= 30 ; $i++): ?>
								<option value="<?php echo $i ?>"><?php echo $i ?></option>
							<?php endfor; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<?php echo Form::button(array('value' => 'Cancel', 'type' => 'Reset' , 'class' => 'button small'))?>
						<?php echo Form::button(array('name' => 'addSubIns','value' => 'Save', 'type' => 'submit' , 'class' => 'button small'))?>
					</td>
				</tr>
			<?php echo Form::close() ?>
		</table>
		</div>
	</div>
</div>