<div class="spacer"></div>
<div class="row">
	<div class="large-12 columns">
		<div class="large-6 large-centered columns">
			<h2>What's the latest news?</h2>
			<hr>
			<br>
			<form name="addNews" action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
				<label for="newsTitle">Headline</label>
				<input type="text" name="newsTitle" id="newsTitle">
				<label for="newsFeed">Content</label>
				<textarea name="newsFeed" id="newsFeed" cols="30" rows="10" no-resize></textarea>
				<input type="submit" name="addNews" value="Post" class="button small right">
			</form>
		</div>
	</div>
</div>