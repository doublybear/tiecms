<div class="spacer"></div>
<div class="row">
	<div class="large-12 columns">
		<div class="large-6 large-centered columns">
			<h2>Add Classroom to the system</h2>
			<hr>
			<br>
			<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
				<label for="roomName">Room Name</label>
				<input type="text" name="roomName" id="roomName">

				<label for="roomCapacity">Room Capacity</label>
				<input type="text" name="roomCapacity" id="roomCapacity">
				
				<input type="submit" value="Save" class="button small right" name="addRoom">
			</form>
		</div>
	</div>
</div>