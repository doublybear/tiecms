<div class="spacer"></div>
<div class="spacer"></div>
<div class="spacer"></div>
<div class="row">
	<div class="small-6 large-centered columns">
		<h1>Admin / Faculty Access page</h1>
	</div>
</div>
<div class="row">
	<div class="rounded-border small-6 large-centered columns">
		<?php echo Form::open(array('action' => htmlentities($_SERVER['PHP_SELF']) , 'method' => 'post', 'class' => 'login-form')) ?>
			<?php echo Form::input('loginUsername' , '' ,array('type' => 'text' , 'placeholder' => 'Enter Username')) ?>
			<?php echo Form::input('loginPassword', '' ,array('type' => 'password' , 'placeholder' => 'Enter Password')) ?>
			<?php echo Form::button(array('name' => 'submitLogin','value' => 'Login', 'type' => 'submit','class' => 'button small'))?>
		<?php echo Form::close() ?>
	</div>
</div>

<div class="spacer"></div>
<div class="spacer"></div>
<div class="spacer"></div>