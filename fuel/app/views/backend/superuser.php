<div class="spacer"></div>
	<div class="supmenu">
		<div class="row">
			<div class="large-12">
				<h2>Admin Menu</h2>
			</div>			
		</div>

		<div class="spacer"></div>

		<div class="row">
			<div class="large-5 small-12 columns">
				<h3>User and Staff</h3>
				<hr>
				<br>
				<ul class="no-bullet su-button">
					<li>
						<a href="add-admin-staff">Add Admin Account</a>
					</li>
					<li>
						<a href="reset-user-password">Reset User Password</a>				
					</li>
					<li>
						<a href="add-student">Add Student</a>				
					</li>
					<li>
						<a href="manage-student-subject">Mange Student Subjects</a>
					</li>
				</ul>
			</div>

			<div class="large-5 small-12 columns">
				<h3>Course and Subjects</h3>
				<hr>
				<br>
				<ul class="no-bullet su-button">
					<li>
						<a href="add-course">Add Course or Subject</a>
					</li>
					<li>
						<a href="manage-course-subject">Manage Course/Subject</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="large-5 small-12 columns">
				<h3>Schedule and Classroom</h3>
				<hr>
				<br>
				<ul class="no-bullet su-button">
					<li>
						<a href="/backend/addrooms">Manage Classroom</a>
					</li>
					<li>
						<a href="/backend/manage-schedule">Manage Schedule</a>				
					</li>
				</ul>
			</div>

			<div class="large-5 small-12 columns">
				<h3>School News</h3>
				<hr>
				<br>
				<ul class="no-bullet su-button">
					<li>
						<a href="/backend/news">Add News to Feed</a>
					</li>
					<li>
						<a href="/backend/manage-announ-news">Manage School News</a>
					</li>
				</ul>
			</div>
		</div>
</div>
<div class="spacer"></div>
<div class="spacer"></div>